//  Copyright © 2022 IceGuye.

// Unless otherwise noted, all files in this project, including but
// not limited to source codes and art contents, are free software:
// you can only redistribute it and/or modify it under the terms of
// the GNU General Public License as published by the Free Software
// Foundation, version 3 of the License.

// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see
// <http://www.gnu.org/licenses/>.

#![deny(unsafe_code)]
#![no_std]
#![no_main]

use cortex_m_rt::entry;
use panic_halt as _;
use stm32f4xx_hal as hal;
use crate::hal::{pac, prelude::*};

#[entry]
fn main() -> ! {
    // Get access to the core peripherals from the cortex-m crate.
    let cp = cortex_m::Peripherals::take().unwrap();
    
    // Get access to the device specific peripherals from the
    // peripheral access crate.
    let dp = pac::Peripherals::take().unwrap();

    let gpiob = dp.GPIOB.split();

    // At STM32 Nucleo-144 boards (MB1137): a red user LED is
    // connected to PB14.
    let mut led = gpiob.pb7.into_push_pull_output();
    let rcc = dp.RCC.constrain();
    let clocks = rcc.cfgr.sysclk(48.MHz()).freeze();
    let mut delay = cp.SYST.delay(&clocks);

    loop {
        // On for 1s, off for 1s.
        led.set_high();
        delay.delay_ms(300_u32);
        led.set_low();
        delay.delay_ms(300_u32);
    }
}
