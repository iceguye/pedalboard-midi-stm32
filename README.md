# pedalboard-midi-stm32

## Introduction

This program is an embeded program to make a STM32 evaluation board
(STM32F429ZIT6U) to become a midi controller of a home-built 32-key
organ pedalboard.

This program intends to use magnetic sensors and convert the strengh
of magnetic field to midi signal.

This program is still in early development. 

## Authors

- IceGuye

    -- Main developer

## Copyright

Copyright © 2022 IceGuye

Unless otherwise noted, all files in this project, including but NOT
limited to source codes and art contents, are free software: you can
only redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation,
version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.